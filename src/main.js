// Relevant video: https://www.youtube.com/watch?v=J2u_TIWPupw

// External dependencies
import { max, min } from 'd3-array';
import { axisBottom, axisLeft } from 'd3-axis';
import { scaleTime } from 'd3-scale';
import { select } from 'd3-selection';
import { timeMonth, timeYear } from 'd3-time';
import { timeFormat, timeParse } from 'd3-time-format';

// Project asset dependencies
import data from './assets/data.json';

const baseTemperature = data['baseTemperature'];
const monthlyVariance = data['monthlyVariance'];

const height = 300;
const width = 1000;
const leftPadding = 70;
const rightPadding = 100;
const topPadding = 60;
const bottomPadding = 60;
const rangeXMin = leftPadding;
const rangeXMax = width - rightPadding;
const rangeWidth = rangeXMax - rangeXMin;
const rangeYMin = topPadding;
const rangeYMax = height - bottomPadding;
const rangeHeight = rangeYMax - rangeYMin;

const parseMonth = timeParse( '%m' );
const parseYear = timeParse( '%Y' );

const formatYear = timeFormat( '%Y' );

const yearMin = min( monthlyVariance, d => parseYear( d['year'] ) );
const yearMax = max( monthlyVariance, d => parseYear( d['year'] ) );

const xScale = scaleTime()
	.domain( [ yearMin, yearMax ] )
	.range( [ rangeXMin, rangeXMax ] );

const yScale = scaleTime()
	.domain( [ parseMonth( 1 ), parseMonth( 12 ) ] )
	.range( [ rangeYMin, rangeYMax ] );

let mouseX, mouseY;

document.addEventListener(
	'mousemove',
	( { pageX, pageY } ) => {
		mouseX = pageX;
		mouseY = pageY;
	}
);

const root = select( '#app-root' );

// Append title to root element.
root.append( 'h1' )
	.attr( 'id', 'title' )
	.text( 'Monthly Global Land-Surface Temperature' );

// Append description to root element.
root.append( 'p' )
	.attr( 'id', 'description' )
	.text( `${ formatYear( yearMin ) }-${ formatYear( yearMax ) }; base temperature: ${ baseTemperature }℃; info source unknown.🤔` );

// Append svg to root element.
const svg = root.append( 'svg' )
	.attr( 'class', 'heat-map' )
	.attr( 'viewBox', `0 0 ${ width } ${ height }` );

const formatMonth = timeFormat( '%B' );

const yAxis = axisLeft( yScale )
	.ticks( timeMonth.every( 1 ) )
	.tickFormat( formatMonth );

// Append y-axis to svg.
svg.append( 'g' )
	.attr( 'id', 'y-axis' )
	.attr( 'transform', `translate(${ rangeXMin - 2 }, ${ rangeHeight / 24 })` )
	.call( yAxis );

const xAxis = axisBottom( xScale )
	.ticks( timeYear.every( 10 ) );

// Append x-axis to svg.
svg.append( 'g' )
	.attr( 'id', 'x-axis' )
	.attr( 'transform', `translate(0, ${ rangeYMax + 16 })` )
	.call( xAxis );

const colorWarmer3 = '#f66';
const colorWarmer2 = '#fa6';
const colorWarmer1 = '#ff6';
const colorCooler1 = '#8ef';
const colorCooler2 = '#7af';
const colorCooler3 = '#66f';

svg.selectAll( '.cell' )
	.data( monthlyVariance )
	.enter()
	.append( 'rect' )
	.attr( 'class', 'cell' )
	// Would have used classes, but inline styles are required for freeCodeCamp challenge tests.
	.attr(
		'fill',
		d => {
			if ( d['variance'] >= 2 )
				return colorWarmer3;
			else if ( d['variance'] >= 1 )
				return colorWarmer2;
			else if ( d['variance'] >= 0 )
				return colorWarmer1;
			else if ( d['variance'] >= -1 )
				return colorCooler1;
			else if ( d['variance'] >= -2 )
				return colorCooler2;
			else
				return colorCooler3;
		}
	)
	// https://www.freecodecamp.org/forum/t/d3-heat-map-project-cant-figure-out-how-to-pass-content-tests-8-and-9/260476/2
	.attr( 'data-month', d => d['month'] - 1 )
	.attr( 'data-year', d => d['year'] )
	// Misleading attribute name, but it's required for freeCodeCamp challenge tests.
	.attr( 'data-temp', d => d['variance'] )
	.attr( 'x', d => xScale( parseYear( d['year'] ) ) )
	.attr(
		'y',
		d =>
			yScale( parseMonth( d['month'] ) )
	)
	.attr( 'width', rangeWidth / ( monthlyVariance.length / 12 ) )
	.attr( 'height', rangeHeight / 12 )
	.on( 'mousemove', function() {
		const year = this.getAttribute( 'data-year' );
		const month = this.getAttribute( 'data-month' );
		const variance = this.getAttribute( 'data-temp' );

		select( '#tooltip' )
			.attr( 'data-year', year )
			.style( 'display', 'block' )
			.style( 'position', 'absolute' )
			.style( 'left', `${ mouseX + 16 }px` )
			.style( 'top', `${ mouseY - 80 }px` )
			.html(
				`${ formatYear( parseYear( year ) ) } - ${ formatMonth( parseMonth( month ) ) }<br />` +
				`${ ( Number( baseTemperature ) + Number( variance ) ).toFixed( 3 ) }℃<br />` +
				`${ variance }℃`
			);
	} )
	.on( 'mouseleave', function() {
		select( '#tooltip' )
			.style( 'display', 'none' );
	} );

const legend = svg.append( 'g' )
	.attr( 'id', 'legend' )
	.attr( 'transform', 'translate(10, 10)' );

legend.append( 'rect' )
	.attr( 'width', 455 )
	.attr( 'height', 24 )
	.style( 'fill', '#111' );

legend.append( 'text' )
	.attr( 'class', 'legend-text' )
	.attr( 'x', 8 )
	.attr( 'y', 12 )
	.attr( 'font-weight', '600' )
	.text( 'Degrees from base temperature' );

// < -2 bar color example
legend.append( 'rect' )
	.attr( 'x', 210 )
	.attr( 'y', 8 )
	.attr( 'width', 8 )
	.attr( 'height', 8 )
	.attr( 'fill', colorCooler3 );

legend.append( 'text' )
	.attr( 'class', 'legend-text' )
	.attr( 'x', 220 )
	.attr( 'y', 13 )
	.text( '< -2' );

// < -1 below bar color example
legend.append( 'rect' )
	.attr( 'x', 250 )
	.attr( 'y', 8 )
	.attr( 'width', 8 )
	.attr( 'height', 8 )
	.attr( 'fill', colorCooler2 );

legend.append( 'text' )
	.attr( 'class', 'legend-text' )
	.attr( 'x', 260 )
	.attr( 'y', 13 )
	.text( '< -1' );

// < 0 below bar color example
legend.append( 'rect' )
	.attr( 'x', 290 )
	.attr( 'y', 8 )
	.attr( 'width', 8 )
	.attr( 'height', 8 )
	.attr( 'fill', colorCooler1 );

legend.append( 'text' )
	.attr( 'class', 'legend-text' )
	.attr( 'x', 300 )
	.attr( 'y', 13 )
	.text( '< 0' );

// >= 0 below bar color example
legend.append( 'rect' )
	.attr( 'x', 330 )
	.attr( 'y', 8 )
	.attr( 'width', 8 )
	.attr( 'height', 8 )
	.attr( 'fill', colorWarmer1 );

legend.append( 'text' )
	.attr( 'class', 'legend-text' )
	.attr( 'x', 340 )
	.attr( 'y', 13 )
	.text( '>= 0' );

// >= 1 below bar color example
legend.append( 'rect' )
	.attr( 'x', 370 )
	.attr( 'y', 8 )
	.attr( 'width', 8 )
	.attr( 'height', 8 )
	.attr( 'fill', colorWarmer2 );

legend.append( 'text' )
	.attr( 'class', 'legend-text' )
	.attr( 'x', 380 )
	.attr( 'y', 13 )
	.text( '>= 1' );

// >= 2 below bar color example
legend.append( 'rect' )
	.attr( 'x', 410 )
	.attr( 'y', 8 )
	.attr( 'width', 8 )
	.attr( 'height', 8 )
	.attr( 'fill', colorWarmer3 );

legend.append( 'text' )
	.attr( 'class', 'legend-text' )
	.attr( 'x', 420 )
	.attr( 'y', 13 )
	.text( '>= 2' );

root.append( 'p' )
	.attr( 'id', 'tooltip' )
	.style( 'display', 'none' );