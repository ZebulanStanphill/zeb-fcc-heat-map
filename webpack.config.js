/* global __dirname, module, require */
const path = require( 'path' );
const { CleanWebpackPlugin } = require( 'clean-webpack-plugin' );
const CopyWebpackPlugin = require( 'copy-webpack-plugin' );
const HtmlWebpackPlugin = require( 'html-webpack-plugin' );
const HtmlWebpackTagsPlugin = require( 'html-webpack-tags-plugin' );

module.exports = {
	mode: 'development',
	entry: './src/main.js',
	output: {
		filename: 'bundle.js',
		path: path.resolve( __dirname, 'dist' )
	},
	devtool: 'source-map',
	module: {
		rules: [
			{
				test: /\.jsx?$/,
				include: [
					path.resolve( __dirname, 'src' )
				],
				use: [
					'babel-loader',
					'eslint-loader'
				]
			},
			{
				test: /\.css$/,
				include: [
					path.resolve( __dirname, 'src' )
				],
				use: [
					'style-loader',
					'css-loader'
				]
			}
		]
	},
	plugins: [
		new CleanWebpackPlugin(),
		new CopyWebpackPlugin(
			[
				{ from: 'src/assets/style.css', to: 'style.css' }
			]
		),
		new HtmlWebpackPlugin( {
			template: './src/assets/main.html'
		} ),
		new HtmlWebpackTagsPlugin( {
			tags: [ 'style.css' ],
			append: true
		} )
	]
};
