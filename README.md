# Zeb’s freeCodeCamp Heat Map

## Description
A heat map made using D3.js for a freeCodeCamp project.

Relevant video: https://www.youtube.com/watch?v=J2u_TIWPupw

## Usage
1. Open a terminal at the project root.
2. Run `npm install` to install the package dependencies.
3. Run `npm run build` to build the project.
4. Open `dist/index.html` to view/use the project.

## Licensing info

© 2019 Zebulan Stanphill

This project is released under the GNU General Public License version 3 (or any later version).

https://www.gnu.org/licenses/gpl-3.0.html

This project loads the testable-projects-fcc script:

https://github.com/freeCodeCamp/testable-projects-fcc

© 2017 freeCodeCamp

Released under the BSD 3-Clause License:

https://github.com/freeCodeCamp/testable-projects-fcc/blob/master/LICENSE.md